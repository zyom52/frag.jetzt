import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SwUpdate } from '@angular/service-worker';
import { NotificationService } from './services/util/notification.service';
import { Rescale } from './models/rescale';
import { CustomIconService } from './services/util/custom-icon.service';
import { filter } from 'rxjs/operators';
import { StartUpService } from './services/util/start-up.service';
import { EventService } from './services/util/event.service';
import {
  LoginDialogRequest,
  LoginDialogResponse, MotdDialogRequest,
  RescaleRequest,
  RescaleResponse,
  sendEvent,
} from './utils/service-component-events';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from './components/shared/login/login.component';
import { MotdDialogComponent } from './components/shared/_dialogs/motd-dialog/motd-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  public static rescale: Rescale = new Rescale();
  private static scrollAnimation = true;
  @ViewChild('headerElement')
  headerElement: ElementRef<HTMLElement>;
  @ViewChild('footerElement')
  footerElement: ElementRef<HTMLElement>;
  @ViewChild('scrollElement')
  scrollElement: ElementRef<HTMLElement>;
  title = 'frag.jetzt';
  private _lastScrollTop = 0;

  constructor(
    private _startUp: StartUpService,
    private translationService: TranslateService,
    private update: SwUpdate,
    public notification: NotificationService,
    private customIconService: CustomIconService,
    private eventService: EventService,
    private dialog: MatDialog,
  ) {
    this.initDialogsForServices();
    customIconService.init();
  }

  public static scrollTop() {
    const sc: HTMLElement = document.getElementById('scroll_container');
    if (AppComponent.scrollAnimation) {
      sc.scrollTo({ top: 0, behavior: 'smooth' });
    } else {
      sc.scrollTop = 0;
    }
  }

  public static isScrolledTop(): boolean {
    return document.getElementById('scroll_container').scrollTop === 0;
  }

  ngOnInit(): void {
    this.update.versionUpdates.pipe(
      filter(e => e.type === 'VERSION_READY'),
    ).subscribe(_ => {
      let install: string;
      this.translationService.get('home-page.install').subscribe(msg => {
        install = msg;
      });
      this.translationService.get('home-page.update-available').subscribe(msg => {
        this.notification.show(msg, install, {
          duration: 5000,
        });
      });
      this.notification.snackRef.afterDismissed().subscribe(info => {
        if (info.dismissedByAction === true) {
          window.location.reload();
        }
      });
    });
  }

  public getRescale(): Rescale {
    return AppComponent.rescale;
  }

  onScroll() {
    const scroller = this.scrollElement.nativeElement;
    const current = scroller.scrollTop;
    if (Math.abs(this._lastScrollTop - current) <= 10 && current > 0) {
      return;
    }
    const header = this.headerElement.nativeElement;
    const footer = this.footerElement.nativeElement;
    if (current > this._lastScrollTop && current > header.offsetHeight) {
      header.style.marginTop = '-' + header.offsetHeight + 'px';
    } else {
      header.style.marginTop = '0';
    }
    const height = scroller.scrollHeight - scroller.clientHeight - footer.offsetHeight;
    if (current > this._lastScrollTop && current < height) {
      footer.style.marginBottom = '-' + footer.offsetHeight + 'px';
    } else {
      footer.style.marginBottom = '0';
    }
    this._lastScrollTop = current;
  }

  private initDialogsForServices() {
    this.eventService.on<LoginDialogRequest>(LoginDialogRequest.name).subscribe(request => {
      const dialogRef = this.dialog.open(LoginComponent, {
        width: '350px'
      });
      dialogRef.componentInstance.redirectUrl = request.redirectUrl;
      dialogRef.afterClosed().subscribe(() => {
        sendEvent(this.eventService, new LoginDialogResponse());
      });
    });
    this.eventService.on<RescaleRequest>(RescaleRequest.name).subscribe(request => {
      let scale;
      if (request.scale === 'initial') {
        scale = this.getRescale().getInitialScale();
      } else {
        scale = request.scale;
      }
      this.getRescale().setScale(scale);
      sendEvent(this.eventService, new RescaleResponse());
    });
    this.eventService.on<MotdDialogRequest>(MotdDialogRequest.name).subscribe(request => {
      const dialogRef = this.dialog.open(MotdDialogComponent, {
        width: '80%',
        maxWidth: '600px',
        minHeight: '95%',
        height: '95%',
      });
      dialogRef.componentInstance.motds = request.motds;
      dialogRef.afterClosed().subscribe(() => {
        sendEvent(this.eventService, new LoginDialogResponse());
      });
    });
  }

}
